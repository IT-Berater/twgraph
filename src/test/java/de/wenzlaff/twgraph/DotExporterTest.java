package de.wenzlaff.twgraph;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.Multigraph;
import org.jgrapht.nio.dot.DOTExporter;
import org.junit.jupiter.api.Test;

/**
 * Kleine Testklasse.
 * 
 * Erzeugt eine DOT Testdatei im target Verzeichnis
 * 
 * <pre>
 strict digraph G {
  1 [ label="JUnit Vertex 1" style="filled" fillcolor="#EEEEEE" color="#31CEF0" ];
  2 [ label="JUnit Vertex 2" style="filled" fillcolor="#EEEEEE" color="#31CEF0" ];
  3 [ label="JUnit Vertex 3" style="filled" fillcolor="#EEEEEE" color="#31CEF0" ];
  4 [ label="JUnit Vertex 4" style="filled" fillcolor="#EEEEEE" color="#31CEF0" ];
  1 -> 2;
  1 -> 3;
  1 -> 4;
  2 -> 3;
  3 -> 4;
}
 * </pre>
 * 
 * @author Thomas Wenzlaff
 *
 */
class DotExporterTest {

	private static final Logger LOG = LogManager.getLogger(DotExporterTest.class);

	/** SUT */
	private DotExporter dotExp = new DotExporter();

	@Test
	void testWriteDotFile() {

		ListenableGraph<Object, DefaultEdge> graph = getTestGraph();

		File dotFile = new File("target", "junit-test.dot");

		DOTExporter<Object, DefaultEdge> writeDotFile = dotExp.writeDotFile(graph, dotFile);

		assertNotNull(writeDotFile);
		assertTrue(dotFile.exists());
	}

	/**
	 * Erzeugt Test Graph.
	 * 
	 * https://jgrapht.org/guide/UserOverview
	 * 
	 * <pre>
	 * Class Name 	            Edges 	    Self-loops 	Multiple edges 	Weighted
	 * DefaultDirectedGraph 	directed 	yes 	    no 	            no
	 * 
	 * directed edges: an edge has a source and a target
	 * self-loops: whether to allow edges which connect a vertex to itself
	 * multiple edges: whether to allow more than one edge between the same vertex pair 
	 *  (note that in a directed graph, two edges between the same vertex pair but with opposite direction do not count as multiple edges)
	 * weighted: whether a double weight is associated with each edge 
	 *  (for these graph types, you’ll usually want to use DefaultWeightedEdge as your edge class); 
	 *  unweighted graphs are treated as if they have a uniform edge weight of 1.0, 
	 *  which allows them to be used in algorithms such as finding a shortest path
	 * </pre>
	 * 
	 * @return
	 */
	private ListenableGraph<Object, DefaultEdge> getTestGraph() {

		ListenableGraph<Object, DefaultEdge> directedGraph = new DefaultListenableGraph<>(new Multigraph<>(DefaultEdge.class));

		String v1 = "JUnit Vertex 1";
		String v2 = "JUnit Vertex 2";
		String v3 = "JUnit Vertex 3";
		String v4 = "JUnit Vertex 4";

		directedGraph.addVertex(v1);
		directedGraph.addVertex(v2);
		directedGraph.addVertex(v3);
		directedGraph.addVertex(v4);

		directedGraph.addEdge(v1, v2);
		directedGraph.addEdge(v1, v3);
		directedGraph.addEdge(v1, v4);
		directedGraph.addEdge(v2, v3);
		directedGraph.addEdge(v3, v4);

		assertEquals(5, directedGraph.iterables().edgeCount());
		assertEquals(4, directedGraph.iterables().vertexCount());

		return directedGraph;
	}
}
