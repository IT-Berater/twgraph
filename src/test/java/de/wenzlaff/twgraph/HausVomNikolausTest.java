package de.wenzlaff.twgraph;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.alg.cycle.HierholzerEulerianCycle;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.nio.dot.DOTExporter;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
class HausVomNikolausTest {

	private static final Logger LOG = LogManager.getLogger(HausVomNikolausTest.class);

	/** SUT. */
	private HausVomNikolaus graph = new HausVomNikolaus();

	@Test
	void testHausVomNikoloaus() {
		assertNotNull(graph);
	}

	@Test
	void testIsHausVomNikolausEulerian() {

		DotExporter dotExp = new DotExporter();

		assertFalse(graph.isHausVomNikolausEulerian());

		File dotFile = new File("target", "junit-test-hausvomnikolaus.dot");

		DOTExporter<Object, DefaultEdge> writeDotFile = dotExp.writeDotFile(graph.getGraph(), dotFile);

		assertNotNull(writeDotFile);
		assertTrue(dotFile.exists());
	}

	@Test
	void testIsEulerian() {

		// https://de.wikipedia.org/wiki/Algorithmus_von_Hierholzer
		// der in einem ungerichteten Graphen Eulerkreise bestimmt
		HierholzerEulerianCycle<Object, DefaultEdge> hier = new HierholzerEulerianCycle<>();
		boolean isEulerian = hier.isEulerian(getTestEulerGraph());

		assertTrue(isEulerian);
	}

	private ListenableGraph<Object, DefaultEdge> getTestEulerGraph() {

		ListenableGraph<Object, DefaultEdge> eulersGraphQuadrat = new DefaultListenableGraph<>(new SimpleGraph<>(DefaultEdge.class));

		eulersGraphQuadrat.addVertex("1");
		eulersGraphQuadrat.addVertex("2");
		eulersGraphQuadrat.addVertex("3");
		eulersGraphQuadrat.addVertex("4");

		eulersGraphQuadrat.addEdge("1", "2");
		eulersGraphQuadrat.addEdge("2", "3");
		eulersGraphQuadrat.addEdge("3", "4");
		eulersGraphQuadrat.addEdge("4", "1");

		return eulersGraphQuadrat;
	}
}
