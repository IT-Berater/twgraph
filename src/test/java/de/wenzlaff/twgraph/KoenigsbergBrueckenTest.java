package de.wenzlaff.twgraph;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.nio.dot.DOTExporter;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
class KoenigsbergBrueckenTest {

	private static final Logger LOG = LogManager.getLogger(KoenigsbergBrueckenTest.class);

	/** SUT. */
	private KoenigsbergBruecken graph = new KoenigsbergBruecken();

	@Test
	void testKoenigsbergBruecken() {
		assertNotNull(graph);
	}

	@Test
	void testIsKoenigsbergerBrueckenproblemLoesbar() {

		DotExporter dotExp = new DotExporter();

		assertFalse(graph.isKoenigsbergerBrueckenproblemLoesbar());

		File dotFile = new File("target", "junit-test-koenigsberger.dot");

		DOTExporter<Object, DefaultEdge> writeDotFile = dotExp.writeDotFile(graph.getGraph(), dotFile);

		assertNotNull(writeDotFile);
		assertTrue(dotFile.exists());

	}
}
