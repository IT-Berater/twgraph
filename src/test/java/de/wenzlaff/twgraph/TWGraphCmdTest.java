package de.wenzlaff.twgraph;

import org.junit.jupiter.api.Test;

/**
 * Parameter Test.
 * 
 * @author Thomas Wenzlaff
 *
 */
class TWGraphCmdTest {

	@Test
	void testaufrufCmdHilfe() throws Exception {

		String[] parmeter = { "-h" };
		TWGraphCmd.main(parmeter);
	}
}
