package de.wenzlaff.twgraph;

import java.awt.Dimension;
import java.io.File;

import javax.swing.JApplet;
import javax.swing.JFrame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultListenableGraph;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.swing.mxGraphComponent;

/**
 * Der Graph der durch dependency:list erzeugt wurde.
 * 
 * Öffnet auch ein Applet mit dem Graph.
 * 
 * @author Thomas Wenzlaff
 */
public class TWGraph extends JApplet {

	private static final long serialVersionUID = 2202072534703043194L;

	private static final Dimension DEFAULT_SIZE = new Dimension(800, 600);

	private JGraphXAdapter<Object, DefaultEdge> jgxAdapter;

	private MavenUtil mavenUtil = new MavenUtil();

	private GraphReader graphReader = new GraphReader();

	private DotExporter dotExporter = new DotExporter();

	private static final Logger LOG = LogManager.getLogger(TWGraph.class);

	private static File outputFile;

	public static void main(String[] args) {
		LOG.debug("Starte TWGraph ...");

		outputFile = new File(args[0], args[1]);

		boolean isApplet = Boolean.valueOf(args[2]);

		TWGraph applet = new TWGraph();
		applet.init();
		if (isApplet) {
			JFrame frame = new JFrame();
			frame.getContentPane().add(applet);
			frame.setTitle("TWGraph");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.pack();
			frame.setVisible(true);
		}
	}

	@Override
	public void init() {

		ListenableGraph<Object, DefaultEdge> graph = new DefaultListenableGraph<>(new DefaultDirectedGraph<>(DefaultEdge.class));

		// create a visualization using JGraph, via an adapter
		jgxAdapter = new JGraphXAdapter<>(graph);

		setPreferredSize(DEFAULT_SIZE);
		mxGraphComponent component = new mxGraphComponent(jgxAdapter);
		component.setConnectable(false);
		component.getGraph().setAllowDanglingEdges(false);
		getContentPane().add(component);
		resize(DEFAULT_SIZE);

		mavenUtil.createDependencyFileList(outputFile);

		graphReader.readLinesFromFile(outputFile, graph);

		dotExporter.writeDotFile(graph, new File("twgraph.dot"));

		// positioning via jgraphx layouts
		mxCircleLayout layout = new mxCircleLayout(jgxAdapter);

		// center the circle
		int radius = 100;
		layout.setX0((DEFAULT_SIZE.width / 2.0) - radius);
		layout.setY0((DEFAULT_SIZE.height / 2.0) - radius);
		layout.setRadius(radius);
		layout.setMoveCircle(true);

		layout.execute(jgxAdapter.getDefaultParent());
	}
}
