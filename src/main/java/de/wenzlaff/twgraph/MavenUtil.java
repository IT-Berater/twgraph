package de.wenzlaff.twgraph;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.maven.cli.MavenCli;

/**
 * Erzeugt eine Liste der Abhängigkeiten mit dem Maven Goal dependency:list.
 * 
 * @author Thomas Wenzlaff
 */
public class MavenUtil {

	private static final Logger LOG = LogManager.getLogger(MavenUtil.class);

	public int createDependencyFileList(File outputFile) {

		// es muss evl. ein .mvn Verz. im Projekt vorhanden sein!
		String[] mavenArgs = new String[] { "dependency:list", "-DexcludeTransitive=false", "-DoutputFile=" + outputFile.getName() };

		System.setProperty(MavenCli.MULTIMODULE_PROJECT_DIRECTORY, outputFile.getParent());
		MavenCli mavenCli = new MavenCli();
		int resultCode = mavenCli.doMain(mavenArgs, outputFile.getParent(), System.out, System.err);

		if (resultCode == 0) {
			LOG.info("Maven command executed successfully. Result in File {}", outputFile.getAbsolutePath());
		} else {
			LOG.error("Maven command {} execution failed in Project {}", mavenArgs[0], outputFile.getAbsolutePath());
		}
		return resultCode;
	}
}