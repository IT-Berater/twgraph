package de.wenzlaff.twgraph;

import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Komandozeile für das TWGraph Programm.
 * 
 * <pre>
Usage: TWGraph [-ah] -o=<dateiName> -p=<projektDir>
Programm zur Erzeugung von Graphen.
  -a, --[no-]applet   Schaltet das Applet aus -a=false
  -h, --help          Zeigt die Hilfe an und beendet das Programm
  -o, --outputfilename=<dateiName>
                      Der Output Dateiname für die Abhängigkeiten
  -p, --projektverzeichnis=<projektDir>
                      Der Pfad zum Projektverzeichnis
Thomas Wenzlaff
http://www.wenzlaff.info
Copyright (c) 2023 Thomas Wenzlaff
 * </pre>
 *
 * @author Thomas Wenzlaff
 */
@Command(name = "TWGraph", mixinStandardHelpOptions = true, version = "TWGraph 1.0.0", description = "Programm zur Erzeugung von Graphen.", showDefaultValues = true, footer = {
		"@|fg(green) Thomas Wenzlaff|@", "@|fg(red),bold http://www.wenzlaff.info|@", "@|fg(red),bold Copyright (c) 2023 Thomas Wenzlaff|@" })
public class TWGraphCmd implements Callable<Integer> {

	private static final Logger LOG = LogManager.getLogger(TWGraphCmd.class);

	@Option(names = { "-p", "--projektverzeichnis" }, description = "Der Pfad zum Projektverzeichnis", required = true)
	private String projektDir;

	@Option(names = { "-o", "--outputfilename" }, description = "Der Output Dateiname für die Abhängigkeiten", required = true)
	private String dateiName;

	@Option(names = { "-a", "--applet" }, description = "Schaltet das Applet aus -a=false", negatable = true, defaultValue = "true", fallbackValue = "true")
	private boolean isApplet;

	@Option(names = { "-h", "--help" }, usageHelp = true, description = "Zeigt die Hilfe an und beendet das Programm")
	boolean help;

	public static void main(String[] args) {

		CommandLine cmd = new CommandLine(new TWGraphCmd());
		cmd.execute(args);
	}

	@Override
	public Integer call() throws Exception {
		LOG.info("TWGraph ... Starte mit Projektverzeichnis {} und Outputdatei: {} Applet Start: {}", projektDir, dateiName, isApplet);

		String[] args = { projektDir, dateiName, String.valueOf(isApplet) };
		TWGraph.main(args);
		return 0;
	}
}
