package de.wenzlaff.twgraph;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Ein Node der durch die Maven GroupId dargestellt wird.
 * 
 * @author Thomas Wenzlaff
 */
public class MavenVertex {

	private static final Logger LOG = LogManager.getLogger(MavenVertex.class);

	/**
	 * Die Maven GroupId z.B. org.apache.maven
	 */
	private final String groupId;

	public MavenVertex(String groupId) {
		LOG.trace("Create MavenVertex mit GroupId {}", groupId);
		this.groupId = groupId;
	}

	public String getGroupId() {
		return groupId;
	}

	@Override
	public String toString() {

		return getGroupId();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof MavenVertex))
			return false;
		MavenVertex other = (MavenVertex) obj;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		return true;
	}
}
