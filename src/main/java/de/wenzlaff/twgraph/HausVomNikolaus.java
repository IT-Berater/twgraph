package de.wenzlaff.twgraph;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.alg.cycle.HierholzerEulerianCycle;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

/**
 * Ziel ist es, ein „Haus“ in einem Linienzug aus genau acht Strecken zu
 * zeichnen, ohne eine Strecke zweimal zu durchlaufen. Begleitet wird das
 * Zeichnen mit dem simultan gesprochenen Reim aus acht Silben: „Das ist das
 * Haus vom Ni-ko-laus.“
 * 
 * https://de.wikipedia.org/wiki/Haus_vom_Nikolaus
 * 
 * @author Thomas Wenzlaff
 */
public class HausVomNikolaus {

	private static final Logger LOG = LogManager.getLogger(HausVomNikolaus.class);

	private ListenableGraph<Object, DefaultEdge> graph;

	public HausVomNikolaus() {
		erstelleHausVomNikolausGraph();
	}

	private void erstelleHausVomNikolausGraph() {

		// Erstelle einen gerichteten Graphen mit SimpleDirectedGraph edges
		graph = new DefaultListenableGraph<>(new SimpleDirectedGraph<>(DefaultEdge.class));

		// Füge 5 Knoten zum Graphen hinzu
		graph.addVertex("1");
		graph.addVertex("2");
		graph.addVertex("3");
		graph.addVertex("4");
		graph.addVertex("5");

		// Füge 8 Kanten = Verbindungsstrecken zwischen den Knoten hinzu
		graph.addEdge("1", "2");
		graph.addEdge("2", "4");
		graph.addEdge("4", "3");
		graph.addEdge("3", "5");
		graph.addEdge("5", "4");
		graph.addEdge("4", "1");
		graph.addEdge("1", "3");
		graph.addEdge("3", "2");

		alleKnotenAusgeben();
	}

	public ListenableGraph<Object, DefaultEdge> getGraph() {
		return graph;
	}

	/**
	 * Problemgegenstand ist ein Graph, für den ein Eulerweg, aber kein Eulerkreis
	 * existiert, da er zwei Knoten von ungeradem Grad (die Knoten 1 und 2 haben
	 * hier jeweils einen Grad von 3) enthält.
	 * 
	 * @return true wenn es ein eulerscher Graph währe, sonst immer false
	 */
	public boolean isHausVomNikolausEulerian() {

		HierholzerEulerianCycle<Object, DefaultEdge> hier = new HierholzerEulerianCycle<>();
		boolean isEulerian = hier.isEulerian(graph); // eigentlich nur für ungerichtete Graphen, geht in diesem Fall aber so auch
		if (isEulerian) {
			LOG.info("Das HausVomNikolaus ist ein eulerscher Graph. Das trifft nie zu.");
		} else {
			LOG.info("Das HausVomNikolaus für den es ein Eulerweg, aber kein Eulerkreis existiert wenn von 1 oder 2 gestartet wird.");
		}
		return isEulerian;
	}

	public List<Object> alleKnotenAusgeben() {

		String startVertex = "1"; // start Knoten

		GraphIterator<Object, DefaultEdge> it = new DepthFirstIterator<>(graph, startVertex);

		List<Object> ergebnisKnoten = new ArrayList<>();

		while (it.hasNext()) {
			ergebnisKnoten.add(it.next());
		}
		LOG.info(ergebnisKnoten);
		return ergebnisKnoten;
	}
}
