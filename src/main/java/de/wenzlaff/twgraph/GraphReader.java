package de.wenzlaff.twgraph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Liese eine durch das Maven Goal dependency:list erzeugte Datei ein.
 * 
 * @author Thomas Wenzlaff
 */
public class GraphReader {

	private static final Logger LOG = LogManager.getLogger(GraphReader.class);

	public void readLinesFromFile(File outputFile, ListenableGraph<Object, DefaultEdge> graph) {

		try (BufferedReader reader = new BufferedReader(new FileReader(outputFile.getAbsoluteFile()))) {
			// Eine Zeile ist wie folgt aufgebaut:
			// GroupID:ArtefactID:Type:Version:Scope -- module GroupID [auto]
			// com.github.vlsi.mxgraph:jgraphx:jar:4.2.2:compile -- module
			// com.github.vlsi.mxgraph.jgraphx [auto]

			graph.addVertex(new MavenVertex("root"));

			String line;
			while ((line = reader.readLine()) != null) {

				String zeilenToken[] = line.split("--");
				if (zeilenToken.length == 2) {
					String groupId = zeilenToken[0].split(":")[0].trim();
					graph.addVertex(new MavenVertex(groupId));

					String nachArtefakt = zeilenToken[1].replace("module ", "").replace("(auto)", "").replace("[auto]", "").trim();
					graph.addVertex(new MavenVertex(nachArtefakt));

					graph.addEdge(new MavenVertex(groupId), new MavenVertex(nachArtefakt));

					graph.addEdge(new MavenVertex("root"), new MavenVertex(groupId));

					LOG.trace("addEdge: " + groupId + " ------------> " + nachArtefakt);
				}
			}
		} catch (IOException e) {
			LOG.error("Error {}", e);
		}
	}
}
