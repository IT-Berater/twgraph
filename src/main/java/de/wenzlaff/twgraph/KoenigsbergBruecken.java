package de.wenzlaff.twgraph;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.alg.cycle.HierholzerEulerianCycle;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.Multigraph;
import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

/**
 * Königsberger Brücken Problem.
 * 
 * Quelle: https://de.wikipedia.org/wiki/K%C3%B6nigsberger_Br%C3%BCckenproblem
 * 
 * @author Thomas Wenzlaff
 */
public class KoenigsbergBruecken {

	private static final Logger LOG = LogManager.getLogger(KoenigsbergBruecken.class);

	private ListenableGraph<Object, DefaultEdge> graph;

	public KoenigsbergBruecken() {
		erstelleKoenigsbergBrueckenProblemGraph();
	}

	/**
	 * Königsberg wird durch den Pregel und seine beiden Inseln geteilt. Die beiden
	 * Stadthälften waren durch je drei Brücken mit den Inseln verbunden, die
	 * untereinander durch eine weitere Brücke verbunden waren.
	 */
	private void erstelleKoenigsbergBrueckenProblemGraph() {

		// Erstelle einen ungerichteten Graphen mit Multiple edges
		graph = new DefaultListenableGraph<>(new Multigraph<>(DefaultEdge.class));

		// Füge 4 Knoten (Landpunkte) zum Graphen hinzu
		graph.addVertex("A");
		graph.addVertex("B");
		graph.addVertex("C");
		graph.addVertex("D");

		// Füge 7 Kanten (Brücken) zwischen den Knoten hinzu
		graph.addEdge("A", "B");
		graph.addEdge("A", "B");
		graph.addEdge("A", "D");
		graph.addEdge("B", "D");
		graph.addEdge("B", "C");
		graph.addEdge("B", "C");
		graph.addEdge("C", "D");

		alleKnotenAusgeben();
	}

	public ListenableGraph<Object, DefaultEdge> getGraph() {
		return graph;
	}

	/**
	 * Die Frage war, ob es einen Weg gibt, bei dem man alle sieben Brücken genau
	 * einmal überquert, und wenn ja, ob auch ein Rundweg möglich ist, bei dem man
	 * wieder zum Ausgangspunkt gelangt.
	 * 
	 * Leonhard Euler bewies 1736, dass ein solcher Weg bzw. „Eulerscher Weg“ in
	 * Königsberg nicht möglich war, da zu allen vier Ufergebieten bzw. Inseln eine
	 * ungerade Zahl von Brücken führte. Es dürfte maximal zwei Ufer (Knoten) mit
	 * einer ungeraden Zahl von angeschlossenen Brücken (Kanten) geben. Diese zwei
	 * Ufer könnten Ausgangs- bzw. Endpunkt sein. Die restlichen Ufer müssten eine
	 * gerade Anzahl von Brücken haben, um sie auch wieder auf einem neuen Weg
	 * verlassen zu können.
	 * 
	 * Durch Kriegseinwirkung und Umbauten nach 1945 ist die ursprüngliche Situation
	 * im heutigen Kaliningrad nicht mehr gegeben. Zwei der zur Insel Kneiphof
	 * führenden Brücken existieren nicht mehr; am nördlichen und südlichen Ufer
	 * enden nur noch jeweils zwei anstatt drei Brücken. Nun ist zwar ein Eulerweg
	 * möglich, jedoch noch immer kein Eulerkreis.
	 * 
	 * @return true wenn das Brückenproblem lösbar ist, sonst false.
	 */
	public boolean isKoenigsbergerBrueckenproblemLoesbar() {

		HierholzerEulerianCycle<Object, DefaultEdge> hier = new HierholzerEulerianCycle<>();
		boolean isEulerian = hier.isEulerian(graph);
		if (isEulerian) { // ([A, B, C, D], [{A,B}, {A,B}, {A,D}, {B,D}, {B,C}, {B,C}, {C,D}])
			LOG.info("Das Königsberger Brückenproblem ist lösbar.");
		} else {
			LOG.info("Das Königsberger Brückenproblem ist nicht lösbar."); // immer nicht lösbar!
		}
		return isEulerian;
	}

	/**
	 * Graph Traversal.
	 * 
	 * Ein DepthFirstIterator ist ein Begriff aus der Informatik und bezieht sich
	 * auf einen Algorithmus zur Durchquerung von Datenstrukturen, insbesondere von
	 * Graphen. Der Algorithmus wird oft verwendet, um alle Elemente oder Knoten
	 * eines Graphen in einer bestimmten Reihenfolge zu besuchen.
	 * 
	 * Bei einem Tiefensuchalgorithmus (Depth-First Search, DFS) wird ein Knoten
	 * besucht und dann rekursiv zu einem unbesuchten Nachbarknoten gegangen, bevor
	 * man zu einem anderen unbesuchten Knoten zurückkehrt. Das bedeutet, dass der
	 * Algorithmus so tief wie möglich in einen Ast des Graphen vordringt, bevor er
	 * sich zurückbewegt und andere Wege erkundet.
	 * 
	 * Ein DepthFirstIterator wäre dementsprechend eine Implementierung eines
	 * solchen Algorithmus als Iteration. Statt Rekursion wird hierbei oft ein
	 * Stapelspeicher (Stack) verwendet, um den Pfadverlauf zu verfolgen. Man
	 * beginnt mit einem Startknoten und fügt dessen Nachbarn in den Stapel ein.
	 * Dann wählt man den obersten Knoten des Stapels aus, entfernt ihn und fährt
	 * mit seinen unbesuchten Nachbarn fort. Dieser Prozess wird so lange
	 * fortgesetzt, bis der Stapel leer ist und somit alle Knoten besucht wurden.
	 * 
	 * Der Tiefensuchalgorithmus und der DepthFirstIterator sind besonders nützlich,
	 * um z.B. Pfadfindungsaufgaben, Netzwerkanalysen und Topologiesortierungen
	 * durchzuführen.
	 * 
	 * @return
	 */
	public List<Object> alleKnotenAusgeben() {

		String startVertex = "A"; // Knoten
		// TopologicalOrderIterator geht nicht, da kein directed Graph
		// BreadthFirstIterator -> C, D, B, A
		// LexBreadthFirstIterator -> A, B, D, C
		// DepthFirstIterator -> A, D, C, B
		GraphIterator<Object, DefaultEdge> it = new DepthFirstIterator<>(graph, startVertex);

		List<Object> ergebnisKnoten = new ArrayList<>();

		while (it.hasNext()) {
			ergebnisKnoten.add(it.next());
		}
		LOG.info(ergebnisKnoten);
		return ergebnisKnoten;
	}
}
