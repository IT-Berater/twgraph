package de.wenzlaff.twgraph;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.ListenableGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;
import org.jgrapht.traverse.DepthFirstIterator;

/**
 * Schreibt eine DOT Datei die mit Graphviz als PNG geschrieben werden kann.
 * 
 * Z.b. mit dot -T png -o test-dot-diagramm.png sample.dot
 * 
 * strict digraph G { 1 [ label="root" style="filled" fillcolor="#EEEEEE"
 * color="#31CEF0" ]; 2 [ label="org.jgrapht" style="filled" fillcolor="#EEEEEE"
 * color="#31CEF0" ]; ...
 * 
 * https://www.graphviz.org/docs/nodes/
 * 
 * @author Thomas Wenzlaff
 *
 */
public class DotExporter {

	private static final Logger LOG = LogManager.getLogger(DotExporter.class);

	public DotExporter() {
	}

	/**
	 * Schreibt einen Graphen in ein dotFile.
	 * 
	 * @param graph   der Graph der in ein File geschrieben wird
	 * @param dotFile der Dateiname des Output DOT File
	 * @return der exportierte Graph mit Attributen
	 */
	public DOTExporter<Object, DefaultEdge> writeDotFile(ListenableGraph<Object, DefaultEdge> graph, File dotFile) {

		DOTExporter<Object, DefaultEdge> exporter = new DOTExporter<>();

		exporter.setVertexAttributeProvider((v) -> {
			Map<String, Attribute> nodeAttribute = new LinkedHashMap<>();
			nodeAttribute.put("label", DefaultAttribute.createAttribute(v.toString()));
			nodeAttribute.put("style", DefaultAttribute.createAttribute("filled"));
			nodeAttribute.put("fillcolor", DefaultAttribute.createAttribute("#EEEEEE"));
			nodeAttribute.put("color", DefaultAttribute.createAttribute("#31CEF0"));
			return nodeAttribute;
		});

		exporter.exportGraph(graph, dotFile);
		LOG.info("DOT Datei {} geschrieben.", dotFile);

		logGraph(graph);

		return exporter;
	}

	/**
	 * Logt den Graph mit Info Level
	 * 
	 * @param directedGraph der Graph der geloggt wird.
	 */
	public void logGraph(ListenableGraph<Object, DefaultEdge> directedGraph) {
		Iterator<Object> iter = new DepthFirstIterator<>(directedGraph);
		while (iter.hasNext()) {
			Object vertex = iter.next();
			LOG.info("Vertex: " + vertex + " is connected to: " + directedGraph.edgesOf(vertex).toString());
		}
	}
}
