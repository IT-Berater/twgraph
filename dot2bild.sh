#!/bin/sh

# Thomas Wenzlaff

inputDateiname="twgraph.dot"

dot -T png -o twgraph-dot-diagramm.png $inputDateiname

circo -T png -o twgraph-circo-diagramm.png $inputDateiname

fdp -T png -o twgraph-fdp-diagramm.png $inputDateiname

