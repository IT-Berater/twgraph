#!/bin/sh

# Thomas Wenzlaff

inputDateiname="./target/junit-test.dot"

dot -T png -o junit-twgraph-diagramm-dot.png $inputDateiname

circo -T png -o junit-twgraph-diagramm-circo.png $inputDateiname

fdp -T png -o junit-twgraph-diagramm-fdp.png $inputDateiname
