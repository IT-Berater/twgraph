#!/bin/sh

# Thomas Wenzlaff

inputDateiname="./target/junit-test-koenigsberger.dot"

dot -T png -o koenigsberger-dot.png $inputDateiname

circo -T png -o koenigsberger-circo.png $inputDateiname

fdp -T png -o koenigsberger-fdp.png $inputDateiname

