#!/bin/sh

# Thomas Wenzlaff

inputDateiname="./target/junit-test-hausvomnikolaus.dot"

dot -T png -o hausvomnikolaus-dot.png $inputDateiname

circo -T png -o hausvomnikolaus-circo.png $inputDateiname

fdp -T png -o hausvomnikolaus-fdp.png $inputDateiname

