# TWGraph

## Königsberger Brückenproblem

### dot

![circo](koenigsberger-dot.png "dot") 


### circo
![circo](koenigsberger-circo.png "circo") 


### fdp
![circo](koenigsberger-fdp.png "fdp") 


## JUnit

### dot
![circo](junit-twgraph-diagramm-dot.png "dot") 


### circo
![circo](junit-twgraph-diagramm-circo.png "circo") 


### fdp
![circo](junit-twgraph-diagramm-fdp.png "fdp") 


## Projekt

### dot
![circo](twgraph-dot-diagramm.png "dot") 


### circo
![circo](twgraph-circo-diagramm.png "circo") 


### fdp
![circo](twgraph-fdp-diagramm.png "fdp") 


